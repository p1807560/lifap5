/* global addQuestionUpdate  renderCurrentQuiz newQuizUpdate htmlNewQuizForm handleQuizUpdateResponse changeFilter state
htmlNewQuestForm closeTooltips htmlUpdateQuiz loadUserBtns htmlFullUserQuiz updateQuiz htmlUpdateQuest updateQuestion renderUpdateQuestOK */

// //////////////////////////////////////////////////////////////////////////////
// RENDUS (partie 2): rendu pour les formulaires divers de modification de quizzes et de questions
// //////////////////////////////////////////////////////////////////////////////

// ////////////////////////////////////////////////////////////////////////////////////////////////
// //////////////////// Rendu de la création et modification de quiz //////////////////////////////

// On fait le rendu du formulaire de création de quiz
// eslint-disable-next-line no-unused-vars
function renderNewQuiz() {
  console.debug("@renderNewQuiz()");

  // On décharge le quiz affiché précédament
  state.currentQuiz = undefined;
  
  // On enlève les tooltips bloqués
  closeTooltips();

  // On force le changement de filtre pour afficher les quizzes de l'utilisateur
  changeFilter("user");

  // On charge le formulaire
  const main = document.getElementById("id-quizzes-main");
  main.innerHTML = htmlNewQuizForm();
  // On marque la saisie en cours pour ne pas supprimer le formulaire en rechargeant
  main.dataset.changing = "";

  // Pour déselectionner les quizzes de la liste si besoin :
  // On récupère tous les éléments de la liste
  const li = Array.from(document.querySelectorAll("#id-quizzes-list li"));
  // On enlève la class "selected" à tous les éléments <li></li>
  li.map((e) => e.classList.remove("selected"));

  // On gère la soumission du formulaire
  document.getElementById("newQuiz").onsubmit = function (ev) {
    ev.preventDefault();
    newQuizUpdate(new FormData(this));
    delete main.dataset.changing;
  };

  // On autorise l'annulation de l'opération si on change de filtre
  document.getElementById("add-cancel").onclick = () => renderCurrentQuiz();

  // On sélectionne le premier champ du formulaire
  document.querySelector("#newQuiz input").select();
}

// On fait le rendu de la modification d'un quiz de l'utilisateur
// eslint-disable-next-line no-unused-vars
function renderUpdateUserQuiz(quiz) {
  console.debug("@renderUpdateUserQuiz()");

  // L'élément principal de la page des quiz utilisateur
  const main = document.getElementById("id-quizzes-main");

  // Si le formulaire n'est pas déja présent
  //      =>  empêche la création de plusieurs formulaires d'ajout de question à la suite
  if (document.getElementById("updateForm") === null) {
    // On charge le formulaire
    main.innerHTML = htmlUpdateQuiz(quiz);
    main.dataset.id = quiz.info.quiz_id;
    main.dataset.changing = "";

    // On désactive les autres boutons
    document.getElementById("addQst").classList.add("disabled");
    document.getElementById("add").classList.add("disabled");

    const openBtn = document.getElementById("openBtn");
    if (openBtn !== null) openBtn.classList.add("disabled");

    const closeBtn = document.getElementById("closeBtn");
    if (closeBtn !== null) closeBtn.classList.add("disabled");

    const updateQuest = Array.from(document.getElementsByClassName("updateQuestBtn"));
    if (updateQuest.length > 0) updateQuest.map((e) => e.classList.add("disabled"));

    const deleteQuest = Array.from(document.getElementsByClassName("deleteQuestBtn"));
    if (deleteQuest.length > 0) deleteQuest.map((e) => e.classList.add("disabled"));

    // On enlève les tooltips bloqués si besoin
    closeTooltips();

    // On charge les données dans le formulaire
    document.querySelector("#updateForm #desc").value = quiz.info.description;
    document.querySelector("#updateForm #titre").value = quiz.info.title;

    // On transforme le bouton en "Annuler"
    const btn = document.getElementById("updateBtn");
    btn.classList.remove("orange", "darken-1");
    btn.classList.add("red");
    btn.firstElementChild.innerHTML = "clear";
    btn.dataset.tooltip = "Annuler";
    btn.onclick = () => renderUpdateUserQuiz(quiz);

    // On charge le handler du formulaire
    document.getElementById("updateForm").onsubmit = (ev) => {
      ev.preventDefault();
      const form = new FormData(ev.target);
      form.append("status", quiz.info.open);
      delete main.dataset.changing;

      return updateQuiz(form, quiz.info.quiz_id).then(handleQuizUpdateResponse);
    };
  } else {
    // On annule l'action

    delete main.dataset.changing;
    // On recharge l'affichage
    main.innerHTML = htmlFullUserQuiz(quiz);
    loadUserBtns(quiz);
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////
// /////////////////// Rendu de la création et modification de question ///////////////////////////

// On fait le rendu de l'ajout d'une question
// eslint-disable-next-line no-unused-vars
function renderNewQuest(quiz) {
  console.debug("@renderNewQuest()");

  closeTooltips();

  const main = document.getElementById("id-quizzes-main");

  // Si le formulaire n'est pas déja présent
  //      =>  empêche la création de plusieurs formulaires d'ajout de question à la suite
  if (document.getElementById("newQuest") === null) {
    // Génère le html
    document.getElementById("quiz-container").innerHTML += htmlNewQuestForm();
    main.dataset.changing = "";

    // On désactive les boutons utilitaires
    document.getElementById("updateBtn").classList.add("disabled");
    document.getElementById("add").classList.add("disabled");

    const openBtn = document.getElementById("openBtn");
    if (openBtn !== null) openBtn.classList.add("disabled");

    const closeBtn = document.getElementById("closeBtn");
    if (closeBtn !== null) closeBtn.classList.add("disabled");

    const updateQuest = Array.from(document.getElementsByClassName("updateQuestBtn"));
    if (updateQuest.length > 0) updateQuest.map((e) => e.classList.add("disabled"));

    const deleteQuest = Array.from(document.getElementsByClassName("deleteQuestBtn"));
    if (deleteQuest.length > 0) deleteQuest.map((e) => e.classList.add("disabled"));

    // On transforme le bouton en bouton "Annuler"
    const Btn = document.getElementById("addQst");
    Btn.classList.remove("green", "lighten-1");
    Btn.classList.add("red");
    Btn.firstElementChild.innerHTML = "clear";
    Btn.dataset.tooltip = "Annuler";

    // Gère la soumission du formulaire
    document.getElementById("newQuest").onsubmit = function submitQuestion(ev) {
      delete main.dataset.changing;
      ev.preventDefault();
      const lastQuestionId = quiz.questions.length - 1;
      const nextQuestionId =
        lastQuestionId >= 0 ? quiz.questions[lastQuestionId].question_id + 1 : 0;
      addQuestionUpdate(
        quiz.info.quiz_id,
        new FormData(this),
        // On prend le dernier indice de quesions utilisé que l'on incrémente pour avoir le nouvel indice
        // On ne prend pas le premier indice libre pour avoir la nouvelle question toujours à la fin
        nextQuestionId
      );
    };

    // Gère l'addition champs de propositions
    document.getElementById("btnProp").onclick = function newProposition() {
      // Identifiant de la nouvelle proposition :
      //   =>  Nombre de propositions présentes dans le formulaire
      const id = document.getElementById("newQuest").children.length - 2;

      // /!\ Création des éléments avec les fonctions JS /!\
      // Pour ne pas faire sauter les évènements associés aux divers éléments dans la page

      // Crée le contenant
      const row = document.createElement("div");
      row.classList.add("row");

      // Crée le bouton radio
      const radioLbl = document.createElement("label");
      const radio = document.createElement("input");
      radio.setAttribute("name", "answer");
      radio.setAttribute("type", "radio");
      radio.setAttribute("tabindex", "-1");
      radio.setAttribute("value", id);
      radioLbl.appendChild(radio);
      const span = document.createElement("span");
      radioLbl.appendChild(span);
      const p = document.createElement("p");
      p.classList.add("col");
      p.classList.add("s1");
      p.classList.add("offset-s1");
      p.classList.add("input-field");
      p.appendChild(radioLbl);
      row.appendChild(p);

      // Crée le champ texte
      const div = document.createElement("div");
      div.classList.add("input-field");
      div.classList.add("col");
      div.classList.add("s9");

      const input = document.createElement("input");
      input.setAttribute("id", id);
      input.setAttribute("name", id);
      input.setAttribute("type", "text");
      input.setAttribute("class", "validate");
      div.appendChild(input);

      const label = document.createElement("label");
      label.setAttribute("for", id);
      label.innerHTML = "Proposition";
      div.appendChild(label);

      row.appendChild(div);

      // Crée la croix pour supprimer le champ
      const btn = document.createElement("p");
      btn.innerHTML = "&times;";
      btn.classList.add("deleteProp");
      btn.classList.add("col");
      btn.classList.add("s1");
      btn.onclick = function removePropField() {
        btn.parentNode.remove();
      };
      row.appendChild(btn);

      // Insère le nouveau bloc dans le formulaire avant le bouton du soumission
      document
        .getElementById("newQuest")
        .insertBefore(row, document.getElementById("newQuestSubmit"));

      // Sélectionne le champ texte
      input.select();
    };

    document.getElementById("content").select();
  } else {
    // On annule l'action
    delete main.dataset.changing;

    // On recharge l'affichage
    main.innerHTML = htmlFullUserQuiz(quiz);
    loadUserBtns(quiz);
  }
}

// On fait le rendu de la modification d'une question
// eslint-disable-next-line no-unused-vars
function renderUpdateQuest(quiz, answer) {
  const main = document.getElementById("id-quizzes-main");

  main.innerHTML = htmlUpdateQuest(quiz, answer);
  main.dataset.changing = "";
  closeTooltips();

  // On désactive les boutons utilitaires
  document.getElementById("updateBtn").classList.add("disabled");
  document.getElementById("add").classList.add("disabled");
  document.getElementById("addQst").classList.add("disabled");

  const openBtn = document.getElementById("openBtn");
  if (openBtn !== null) openBtn.classList.add("disabled");

  const closeBtn = document.getElementById("closeBtn");
  if (closeBtn !== null) closeBtn.classList.add("disabled");

  const updateQuest = Array.from(document.getElementsByClassName("updateQuestBtn"));
  if (updateQuest.length > 0) updateQuest.map((e) => e.classList.add("disabled"));

  const deleteQuest = Array.from(document.getElementsByClassName("deleteQuestBtn"));
  if (deleteQuest.length > 0) deleteQuest.map((e) => e.classList.add("disabled"));

  // Pour annuler l'action
  document.getElementById("cancel").onclick = () => {
    delete main.dataset.changing;
    return renderCurrentQuiz(quiz);
  };

  // Pour gérer la soumission du formulaire
  document.getElementById("updateQuestForm").onsubmit = (ev) => {
    delete main.dataset.changing;
    ev.preventDefault();
    // On soumet le formulaire
    return updateQuestion(new FormData(ev.target), quiz.info.quiz_id, answer.question_id).then(
      renderUpdateQuestOK
    );
  };
}
