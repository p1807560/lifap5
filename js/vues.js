/* eslint-disable array-callback-return */
/* global state loadTooltips deleteQuiz getQuizData htmlQuizzesListGlobal htmlFullQuiz openQuiz closeQuiz renderUpdateUserQuiz htmlQuizSort loadSelect search
htmlFullUserQuiz htmlAddBtn renderNewQuest renderNewQuiz getQuizzes htmlLogInForm htmlUserInfo submitQuestion renderAnswerOK closeTooltips 
renderUpdateQuest getUserQuizAnswer renderDeleteOK updateUser deleteQuestion renderDeleteQuestOK deleteAnswer renderDeleteAnswerOK htmlQuizFilter */

// //////////////////////////////////////////////////////////////////////////////
// RENDUS (partie 1): mise en place du HTML dans le DOM et association des événements pour les quizzes statiques
// //////////////////////////////////////////////////////////////////////////////

// ////////////////////////////////////////////////////////////////////////////////////////////
// ///////////////////////////////// Fonctions utilitaires ////////////////////////////////////

// Pour gérer la suppression d'un quiz utilisateur
function deleteHandler(ev) {
  // On ne déclenche pas le onclick de son parent
  // => On n'affiche pas le quiz qu'on supprime
  ev.stopPropagation();

  const id = Number(ev.target.parentElement.dataset.quizzid);
  if (state.currentQuiz !== undefined && state.currentQuiz.info.quiz_id === id)
    state.currentQuiz = undefined;
  // On supprime le quiz
  return deleteQuiz(id).then(renderDeleteOK);
}

// //////////////////////////////////////////////////////////////////////////////
// //////////////////// Rendu des menus d'organisation //////////////////////////

// Fait le rendu du menu de filtrage des quizzes (remplace les onglets)
function renderQuizFilter() {
  // On charge le menu pour filtrer
  document.getElementById("id-quizzes-filter").innerHTML = htmlQuizFilter(state.selection);

  // On charge le callback pour trier les quizzes
  const btn = Array.from(document.querySelectorAll('input[name="filter"]'));
  btn.map((e) => {
    e.onchange = () => {
      changeFilter(e.value);
    };
  });
}

// Une fonction pour changer de filtre sélectionné
//  value : valeur du filtre
function changeFilter(value) {
  // Si value a une mauvaise valeur
  if (value !== "global" && value !== "user" && value !== "answers") {
    console.error("Erreur : changeFilter : mauvaise sélection");
  } else {
    // On décoche les mauvaises sélections si besoin
    const toUnCheck = document.querySelector(
      `input[name="filter"]:not([value="${value}"]):checked`
    );
    if (toUnCheck !== null) toUnCheck.checked = false;

    // On coche la nouvelle sélection si besoin
    const toCheck = document.querySelector(`input[name="filter"][value="${value}"]:not(:checked)`);
    if (toCheck !== null) toCheck.checked = true;

    // On sauvegarde la sélection dans l'état et on recharge le rendu des quizzes
    state.selection = value;
    renderQuizzes();
  }
}

// Fait le rendu du menu de triage des quizzes
function renderQuizSort() {
  // On charge le rendu des quizzes
  const sort = document.getElementById("id-quizzes-sort");
  sort.innerHTML = htmlQuizSort();

  // On sélectionne les bonnes valeurs
  document
    .querySelector(`select#limit [value="${state.settings.limit}"]`)
    .setAttribute("selected", "");
  document
    .querySelector(`select#order [value="${state.settings.order}"]`)
    .setAttribute("selected", "");
  document.querySelector(`select#dir [value="${state.settings.dir}"]`).setAttribute("selected", "");

  // Initialisation de materialize
  loadSelect();

  // Chargement du callback
  Array.from(document.querySelectorAll("#id-quizzes-sort select")).map((select) => {
    select.onchange = () => {
      // On change l'état puis on recharge le rendu des quizzes
      state.settings[select.id] = select.value;
      refreshQuizzes();
    };
  });
}

// On trie une liste de quizzes
function sortQuizzes(quizzes) {
  const reverse = state.settings.dir === "desc";
  const criteria = state.settings.order;

  let array;
  switch (criteria) {
    case "quiz_id":
      array = quizzes.sort((e1, e2) => e1.quiz_id > e2.quiz_id);
      break;
    case "created_at":
      array = quizzes.sort((e1, e2) => new Date(e1.created_at) > new Date(e2.created_at));
      break;
    case "title":
      array = quizzes.sort((e1, e2) => e1.title > e2.title);
      break;
    case "owner_id":
      array = quizzes.sort((e1, e2) => e1.owner_id > e2.owner_id);
      break;
    default:
      console.error(`ERROR sortQuizzes : wrong value ${criteria}`);
  }
  if (reverse) array.reverse();
  return array;
}

// Fait le rendu du résultat de la recherche
function renderSearch() {
  const liElts = Array.from(document.querySelectorAll("#id-quizzes-list > ul > li"));
  liElts.map((elt) => elt.removeAttribute("style"));

  if (state.searchResult !== undefined)
    state.searchResult.map((result) => {
      const elt = liElts.find((e) => Number(e.dataset.quizzid) === result.quiz_id);

      if (elt !== undefined) {
        elt.setAttribute("style", "background-color: lightgreen !important");
      }
    });
}

// Pour charger la barre de recherche
function loadSearch() {
  // le timeout pour attendre la fin de l'entrée de l'utilisateur
  let timeout;

  // On empêche le rechargement de la page...
  document.getElementById("form-search").onsubmit = (ev) => ev.preventDefault();

  const searchBar = document.getElementById("search");
  searchBar.onkeyup = () => {
    // On réinitialise le timeout
    clearTimeout(timeout);
    // Après 0.5 sec, on lance la recherche si elle n'est pas vide
    timeout = setTimeout(() => {
      if (searchBar.value !== "") return search(searchBar.value).then(renderSearch);
      // Si la barre de recherche est vide, on réinitialise la recherche
      state.searchResult = [];
      return renderSearch();
    }, 500);
  };

  // On charge le bouton pour vider la barre de recherche
  const clear = document.getElementById("clear-search");
  clear.onclick = function clearSearchBar() {
    searchBar.value = "";
    state.searchResult = [];
    renderSearch();
  };
}

// //////////////////////////////////////////////////////////////////////////////
// //////////////////////////// Rendu des quizzes ///////////////////////////////

// Sélectionne le jeu de quiz à afficher (selon le filtre appliqué)
function selectQuizzes() {
  switch (state.selection) {
    case "global":
      return state.quizzes;
    case "user":
      return state.userQuizzes;
    case "answers":
      return state.userAnswers;
    default:
      console.error(state.selection);
      return undefined;
  }
}

// Gère une pagination de résultats
function pagination(quizzes) {
  if (quizzes.localPagination !== undefined) {
    quizzes.pageSize = Number(state.settings.limit);
    quizzes.nbPages = Math.ceil(quizzes.nbResults / quizzes.pageSize);

    // On n'affiche pas de page au delà de la page maximale
    // (utile quand on change la limite)
    if (quizzes.currentPage > quizzes.nbPages) {
      quizzes.currentPage = quizzes.nbPages;
    }

    // On sélectionne que les quizzes faisant partie de la "page"
    quizzes.results = sortQuizzes(quizzes.quizzes).slice(
      (quizzes.currentPage - 1) * quizzes.pageSize,
      quizzes.currentPage * quizzes.pageSize
    );
  }
  return quizzes;
}

// Gère le rendu de la liste de quiz global et associe les handlers aux différents évènements
//  id : id du quiz à afficher
// eslint-disable-next-line no-unused-vars
function renderQuizzes() {
  console.debug(`@renderQuizzes()`);

  // Sélection du jeu de quizzes
  const quizzes = selectQuizzes();

  // On affiche le menu de filtrage
  renderQuizFilter();

  // On affiche le menu de tri
  renderQuizSort(quizzes);

  // Le conteneur pour la liste des quizz
  const ulElt = document.getElementById("id-quizzes-list");

  // On affiche la page, avec la pagination
  ulElt.innerHTML = htmlQuizzesListGlobal(pagination(quizzes));

  if (quizzes.localPagination !== undefined) {
    // Le handler des boutons de pagination
    // eslint-disable-next-line no-inner-declarations
    function clickBtnPager() {
      // Remet à jour les données de state en demandant la page identifiée dans l'attribut data-page
      // Puis lance le rendu
      quizzes.currentPage = Number(this.dataset.page);
      delete document.getElementById("id-quizzes-main").dataset.id;
      refreshQuizzes();
    }

    // Les éléments "boutons" pour changer de page
    const paginationElts = Array.from(document.getElementsByClassName("pagination-elt"));
    paginationElts.map((e) => (e.onclick = clickBtnPager));
  } else {
    // Si on affiche une liste de quizzes avec la pagination gérée par le serveur

    // Le handler des boutons de pagination
    // eslint-disable-next-line no-inner-declarations
    function clickBtnPager() {
      // Remet à jour les données de state en demandant la page identifiée dans l'attribut data-page
      // Puis lance le rendu
      state.quizzes.currentPage = this.dataset.page;
      delete document.getElementById("id-quizzes-main").dataset.id;
      refreshQuizzes();
    }

    // Les éléments "boutons" pour changer de page
    const paginationElts = Array.from(document.getElementsByClassName("pagination-elt"));
    paginationElts.map((e) => (e.onclick = clickBtnPager));
  }

  // Handler pour afficher un quiz de la liste
  // On peut appeler cette fonction "manuellement" pour raffraichir les quiz
  // En lui passant l'id du quiz à afficher
  async function clickQuiz(ev) {
    // Récupère l'id du quiz
    const quizzId =
      ev !== undefined ? Number(this.dataset.quizzid) : state.currentQuiz.info.quiz_id;
    console.debug(`@clickQuizGlobal(${quizzId})`);

    // Pour la sélection visuelle du quiz actuel dans la liste
    // On récupère tous les éléments de la liste
    const li = Array.from(document.querySelectorAll("#id-quizzes-list li"));
    // On enlève la class "selected" à tous les éléments <li></li>
    li.map((e) => e.classList.remove("selected"));
    // On ajoute la classe "selected" à l'élément sur lequel on vient de cliquer
    const liElt = li.find((e) => Number(e.dataset.quizzid) === quizzId);
    if (liElt !== undefined) liElt.classList.add("selected");

    // Array des quizzes
    const quizList = quizzes.results !== undefined ? quizzes.results : quizzes.quizzes;

    // On cherche les informations du quiz dans l'état
    // On a la liste des infos des quizzes avec une fermeture
    const qInfo = quizList.find((e) => e.quiz_id === Number(quizzId));

    // Si il y a des questions à récupérer, on le fait
    //  Si questions_number === undefined
    //    Alors on a répondu au quiz => il y a des questions à récupérer
    const qData =
      qInfo.questions_number > 0 || qInfo.questions_number === undefined
        ? await getQuizData(quizzId)
        : [];

    // Puis on scrolle vers le quiz
    document.getElementById("id-quizzes-main").scrollIntoView({ behavior: "smooth", block: "end" });

    // On lance le rendu du quiz, en donnant les réponses si il y en a
    return renderCurrentQuiz({ info: qInfo, questions: qData });
  }

  // Array des quizzes à afficher
  const quizArray = quizzes.results !== undefined ? quizzes.results : quizzes.quizzes;

  // On charge un quiz ou on réinitialise l'affichage principal
  if (document.getElementById("id-quizzes-main").dataset.changing === undefined) {
    if (
      state.currentQuiz !== undefined &&
      quizArray !== undefined &&
      quizArray.find((e) => e.quiz_id === Number(state.currentQuiz.info.quiz_id)) !== undefined
    )
      clickQuiz();
    else renderCurrentQuiz();
  }

  // La liste de tous les quizzes individuels affichés
  const quizzesElt = document.querySelectorAll("#id-quizzes-list > ul.collection > li");

  // Pour chaque quiz,
  Array.from(quizzesElt).map((q) => {
    // On lui associe son handler de click
    q.onclick = clickQuiz;
  });

  // Pour supprimer les quizzes utilisateurs
  Array.from(document.getElementsByClassName("delete")).map((q) => {
    q.onclick = deleteHandler;
  });

  // On charge la barre de recherche
  loadSearch();

  // On fait le traitement du résultat de la recherche si besoin est
  renderSearch();
}

// On fait le rendu du quiz sélectioné
// eslint-disable-next-line no-unused-vars
function renderCurrentQuiz(data) {
  console.debug(`@renderCurrentQuiz()`);

  closeTooltips();

  // Element principal de la page globale
  const main = document.getElementById("id-quizzes-main");
  // On autorise le rechargement de la page
  delete main.dataset.changing;

  if (data === undefined || data.info === undefined) {
    // Si on n'a pas de données, on affiche une page vide
    state.currentQuiz = undefined;

    // Si l'utilisateur est connecté, on met le bouton d'ajout de quiz
    if (state.user !== undefined) main.innerHTML = htmlAddBtn();
    else main.innerHTML = "";

    // On enlève le marqueur indiquant le quiz affiché si il existe
    delete main.dataset.id;
  } else {
    // Si on a un quiz à afficher
    state.currentQuiz = data;

    // Si le quiz appartient à l'utilisateur
    if (state.user !== undefined && data.info.owner_id === state.user.user_id) {
      main.innerHTML = htmlFullUserQuiz(data);
      main.dataset.id = data.info.quiz_id;
      loadTooltips();
    } else {
      // Si c'est un quiz "lambda"

      // On regarde si il y a des réponses déja données pour ce quiz
      const qAnswers =
        state.userAnswers.quizzes !== undefined
          ? state.userAnswers.quizzes.find((e) => e.quiz_id === Number(data.info.quiz_id))
          : undefined;
      const answers = qAnswers !== undefined ? qAnswers.answers : undefined;

      main.innerHTML = htmlFullQuiz(data, answers);
      // Si l'utilisateur est connecté, on met le bouton d'ajout de quiz
      if (state.user !== undefined) main.innerHTML += htmlAddBtn();

      // On note l'identifiant du quiz affiché
      main.dataset.id = data.info.quiz_id;

      // Si on change un des boutons radio, on soumet la réponses sélectionnée au serveur
      const radio = Array.from(document.querySelectorAll("#id-quizzes-main #quizz input"));
      radio.map((e) => {
        e.onchange = () => {
          return submitQuestion([e.name, e.value], data.info.quiz_id).then((res) =>
            renderAnswerOK(res)
          );
        };
      });
    }
  }

  // On charge les boutons de l'utilisateur
  loadUserBtns(data);
}

// Wrapper pour raffraichir les quiz de l'onglet global
// eslint-disable-next-line no-unused-vars
function refreshQuizzes() {
  return getQuizzes().then(() => {
    return renderQuizzes();
  });
}

// ////////////////////////////////////////////////////////////////////////////////////////////
// //////////////////////// Rendu de des quiz de l'utilisateur ///////////////////////

// On charge les boutons utilitaires de l'utilisateur
function loadUserBtns(quiz) {
  // On associe le handler au bouton d'ajout de question
  const questBtn = document.getElementById("addQst");
  if (questBtn !== null) questBtn.onclick = () => renderNewQuest(quiz);

  // On associe son handler au bouton de mise à jour du quiz
  const updateBtn = document.getElementById("updateBtn");
  if (updateBtn !== null) updateBtn.onclick = () => renderUpdateUserQuiz(quiz);

  // On associe le handler au bouton d'ouverture du quiz
  const openBtn = document.getElementById("openBtn");
  if (openBtn !== null) openBtn.onclick = () => openQuiz(quiz);

  // On associe le handler au bouton de fermeture du quiz
  const closeBtn = document.getElementById("closeBtn");
  if (closeBtn !== null) closeBtn.onclick = () => closeQuiz(quiz);

  // On associe son handler au bouton d'ajout de quiz
  const btnAdd = document.getElementById("add");
  if (btnAdd !== null) btnAdd.onclick = () => renderNewQuiz();

  // On initialise les boutons de modification des questions
  const updateQuest = Array.from(document.getElementsByClassName("updateQuestBtn"));
  if (updateQuest.length > 0)
    updateQuest.map((e) => {
      e.onclick = () =>
        getUserQuizAnswer(quiz.info.quiz_id, e.dataset.id).then((res) =>
          renderUpdateQuest(quiz, res)
        );
    });

  // On initialise les boutons de suppression des questions
  const deleteQuest = Array.from(document.getElementsByClassName("deleteQuestBtn"));
  if (deleteQuest.length > 0)
    deleteQuest.map((e) => {
      e.onclick = () => deleteQuestion(quiz.info.quiz_id, e.dataset.id).then(renderDeleteQuestOK);
    });

  // On initialise les boutons de suppression des questions
  const deleteAnswerBtn = Array.from(document.getElementsByClassName("deleteAnswerBtn"));
  if (deleteAnswerBtn.length > 0)
    deleteAnswerBtn.map((e) => {
      e.onclick = () => deleteAnswer(quiz.info.quiz_id, e.dataset.id).then(renderDeleteAnswerOK);
    });
}

// ////////////////////////////////////////////////////////////////////////////////////////////
// ////////////////////////////// Rendu du bouton de connexion ////////////////////////////////

// On fait le rendu du bouton de connexion
// eslint-disable-next-line no-unused-vars
function renderUserBtn() {
  console.debug("@renderUserBtn()");

  // On récupère les différents éléments de la page dont on aura besoin
  const btn = document.getElementById("id-login");
  const modal = document.getElementById("id-modal-quizz-menu");
  const redBtn = document.getElementById("redBtn");
  const greenBtn = document.getElementById("greenBtn");

  // On ajoute les classes pour déclancher le modal
  btn.classList.add("modal-trigger");
  btn.dataset.target = "id-modal-quizz-menu";

  // On affiche le bon icone selon la connection ou non de l'utilisateur
  if (state.user) btn.innerHTML = "face";
  else btn.innerHTML = "lock_open";

  // On gère le clic sur le bouton
  btn.onclick = () => {
    if (state.user) {
      // Si l'utilisateur est connecté, on affiche les infos utilisateur
      modal.children[1].innerHTML = htmlUserInfo(state.user);

      // On met le bon bouton
      greenBtn.style.display = "none";
      redBtn.innerHTML = "Log Out";
      redBtn.style.display = "block";

      // On gère la déconnexion
      redBtn.onclick = () => {
        state.xApiKey = "";
        state.user = undefined;
        btn.innerHTML = "lock_open";
        updateUser();
      };

      greenBtn.onclick = undefined;

      modal.style.width = "15%";
    } else {
      // Si l'utilisateur n'est pas connecté, on affiche le formulaire de connexion
      modal.children[1].innerHTML = htmlLogInForm();

      // On affiche le bon bouton
      greenBtn.innerHTML = "Log In";
      greenBtn.style.display = "block";
      redBtn.style.display = "none";

      // On gère la connexion
      greenBtn.onclick = () => {
        state.xApiKey = document.getElementById("apiKey").value;
        btn.innerHTML = "face";
        updateUser();
      };

      redBtn.onclick = undefined;
      modal.style.width = "30%";
    }
  };
}

// TODO notes : /quizzes​/{quiz_id}​/questions​/{question_id}​/answers​/ rroute to get answers given by users
// TODO : update ReadMe
// TODO : no API key in git !
