/* global M updateUser installWebSocket callbkTreatment refreshAll */

// //////////////////////////////////////////////////////////////////////////////
// PROGRAMME PRINCIPAL
// //////////////////////////////////////////////////////////////////////////////

// Lancement de l'application
function app() {
  console.debug("@app()");
  // pour installer le websocket
  installWebSocket(callbkTreatment(refreshAll));
  // On lance la connexion de l'utilisateur
  return updateUser().then(() => console.debug(`@app(): OK`));
}

// /////////////////////////////////////////////////////////////////////////////////////
// //////////////////////// Pour gérer Materialize (css) ///////////////////////////////

// Pour initialiser les tooltips
// eslint-disable-next-line no-unused-vars
function loadTooltips() {
  const tooltips = document.querySelectorAll(".tooltipped");
  M.Tooltip.init(tooltips);
}

// Pour fermer les tooltips manuellement (ils restent parfois bloqués :-( )
// eslint-disable-next-line no-unused-vars
function closeTooltips() {
  Array.from(document.getElementsByClassName("material-tooltip")).map(
    (e) => (e.style.display = "none")
  );
}

// Pour initialiser les <select> de Materialise
// eslint-disable-next-line no-unused-vars
function loadSelect() {
  const elems = document.querySelectorAll("select");
  M.FormSelect.init(elems);
}

// /////////////////////////////////////////////////////////////////////////////////////
// //////////////////////// Lancement du programme /////////////////////////////////////

// pour initialiser la bibliothèque Materialize
// https://materializecss.com/auto-init.html
M.AutoInit();

// Lancement de l'application
app();