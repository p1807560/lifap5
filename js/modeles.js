/* eslint-disable no-plusplus */
/* eslint-disable camelcase */
/* globals renderNoAnswer handleNewQuizResponse renderNewQuestOK handleGetUser handleQuizUpdateResponse */

// //////////////////////////////////////////////////////////////////////////////
// LE MODELE : gère les interactions avec le serveur
// //////////////////////////////////////////////////////////////////////////////

// ////////////////////////////////////////////////////////////////////////////////////////////////
// ////////////////////////////////// L'état global de l'application //////////////////////////////

// un objet global pour encapsuler l'état de l'application
// on pourrait le stocker dans le LocalStorage par exemple
const state = {
  // la clef de l'utilisateur
  xApiKey: "",

  // l'URL du serveur où accéder aux données
  serverUrl: "https://lifap5.univ-lyon1.fr",
  // serverUrl: "http://localhost:3000",

  // l'URL du stream pour la websocket
  getStreamUrl: () => {
    const host = new URL(state.serverUrl).hostname;
    return host === "localhost" ? "ws://localhost:3000/stream/" : `wss://${host}:443/stream/`;
  },

  // Les informations de l'utilisteur
  user: undefined,

  // Les quizzes
  quizzes: [],

  // Les quizzes de l'utilisateur
  userQuizzes: [],

  // Les réponses de l'utilisateur
  userAnswers: [],

  // Les résultats de la recherche
  searchResult: [],

  // L'état du filtre
  selection: "global",

  // Les données du quiz affiché
  currentQuiz: undefined,

  // Génère les headers pour le fetch à la volée (pour pouvoir changer l'uilisateur = state.xApiKey)
  getHeaders() {
    const headers = new Headers();
    headers.set("X-API-KEY", this.xApiKey);
    headers.set("Accept", "application/json");
    headers.set("Content-Type", "application/json");
    return headers;
  },

  // Les réglages pour l'affichage des quizzes
  settings: {
    limit: 50,
    order: "quiz_id",
    dir: "asc",
  },
};

// ////////////////////////////////////////////////////////////////////////////////////////////////
// //////////////////////////////////// Outils génériques /////////////////////////////////////////

// un filtre simple pour récupérer les réponses HTTP qui correspondent à des
// erreurs client (4xx) ou serveur (5xx)
function filterHttpResponse(response) {
  return response
    .json()
    .then((data) => {
      if (response.status >= 400 && response.status < 600) {
        throw new Error(`${data.name}: ${data.message}`);
      }
      return data;
    })
    .catch((err) => console.error(`Error : filterHhttpResponse : ${err}`));
}

// Un décorateur pour limiter le nombre d'appels à la fonction fetch
// Un grand merci à Antoine Bulliffon qui a rendu possible cette fonction grace à ses indications et explications !!
//  func : fonction renvoyant une promesse dont on veut limiter les appels
function requestLimiter(func) {
  const queue = []; // Queue d'appels à func à traiter
  const limit = 7; // limite d'appels à func par seconde
  let count = 0; // Compteur

  // On traite la tache suivante
  function dequeue() {
    // Si la limite n'est pas atteinte
    if (count < limit) {
      // On retire le premier élément de la queue
      const task = queue.shift();
      if (task !== undefined) {
        // Si la queue n'est pas vide

        // On augmente le compteur
        count++;

        // Et on lance la requête
        func(...task.data)
          .then((val) => {
            // Dans 1 seconde, on décrémente le compteur et on lance la tache suivante
            setTimeout(() => {
              count--;
              dequeue();
            }, 1000);

            // Et on résout la promesse (succès)
            return task.res(val);
          })
          .catch((val) => {
            // Dans 1 seconde, on décrémente le compteur et on lance la tache suivante
            setTimeout(() => {
              count--;
              dequeue();
            }, 1000);

            // Et on résout la promesse (echec à transmettre en aval de la chaine correspondantes)
            return task.rej(val);
          });
      }
    }
    // Si la limite est atteinte ou si la queue est vide, on ne fait rien
    return false;
  }

  // Construcion de la nouvelle fonction
  return function newFunction(...args) {
    // On retourne une nouvelle promesse
    return new Promise((resolve, reject) => {
      // On sauvegarde les arguments passés et les 2 callbacks de la promesse pour pouvoir la résoudre
      const obj = { data: args, res: resolve, rej: reject };
      // On ajoute à la queue
      queue.push(obj);
      // On lance la résolution
      dequeue();
    });
  };
}

// Pour lancer les requêtes sans se soucier du rate limiter
const request = requestLimiter(fetch);

// ////////////////////////////////////////////////////////////////////////////////////////////////
// ////////////////////////////////// Données de l'utilisateur ////////////////////////////////////

// Un filtre qui filtre les réponses du serveur pour la connection de l'utilisateur
//    => utilisateur pas connecté (retour 401) n'est pas une erreur
function filterConnectionGetUser(response) {
  return response.json().then((data) => {
    if (response.status !== 200 && response.status !== 401) throw new Error("Error on whoami");
    // 401 is not an error, user is not connected
    else if (response.status === 401) return undefined;
    // 200 : user is connected
    else return data;
  });
}

// mise-à-jour asynchrone de l'état avec les informations de l'utilisateur
// l'utilisateur est identifié via sa clef X-API-KEY lue dans l'état
// eslint-disable-next-line no-unused-vars
const getUser = () => {
  console.debug(`@getUser()`);

  const url = `${state.serverUrl}/users/whoami`;
  return request(url, { method: "GET", headers: state.getHeaders() })
    .then(filterConnectionGetUser)
    .then((data) => {
      // /!\ ICI L'ETAT EST MODIFIE /!\
      state.user = data;

      return data;
    })
    .catch((err) => console.error(`ERROR : GetUser ${err}`));
};

// Wrapper pour changer / charger l'utilisateur
// eslint-disable-next-line no-unused-vars
const updateUser = () => getUser().then(handleGetUser);

// Télécharge les quiz de l'utilisateur
// eslint-disable-next-line no-unused-vars
const getUserQuizzes = () => {
  console.debug(`@getUserQuizzes()`);

  const url = `${state.serverUrl}/users/quizzes`;

  return request(url, { method: "GET", headers: state.getHeaders() })
    .then(filterHttpResponse)
    .then((data) => {
      const old = state.userQuizzes;
      const obj = {quizzes:  data};

      obj.pageSize = Number(state.settings.limit);
      obj.currentPage = old.currentPage !== undefined? old.currentPage : 1;
      obj.nbResults = data.length;
      obj.nbPages = Math.ceil(obj.nbResults / obj.pageSize);
      obj.localPagination = true;

      state.userQuizzes = obj;
      return data;
    });
};

// Télécharge les réponses à la question 'qID' d'un quiz 'id' appartenant à l'utilisateur
// eslint-disable-next-line no-unused-vars
function getUserQuizAnswer(id, qID) {
  console.debug(`@getUserQuizData(${id})`);

  const url = `${state.serverUrl}/quizzes/${id}/questions/${qID}/answers`;

  return request(url, { method: "GET", headers: state.getHeaders() }).then(filterHttpResponse);
}

// ////////////////////////////////////////////////////////////////////////////////////////////////
// //////////////////////////////////// Données des Quizzes /////////////////////////////////////////

// getQuizzes télécharge la page 'p' des quizzes et la met dans l'état
// eslint-disable-next-line no-unused-vars
const getQuizzes = () => {
  console.debug(`@getQuizzes()`);

  const page = state.quizzes.currentPage !== undefined ? state.quizzes.currentPage : 1;
  const url = `${state.serverUrl}/quizzes/?page=${page}&limit=${state.settings.limit}&order=${state.settings.order}&dir=${state.settings.dir}`;

  // le téléchargement est asynchrone, renvoi d'une promesse
  return request(url, { method: "GET", headers: state.getHeaders() })
    .then(filterHttpResponse)
    .then((data) => {
      state.quizzes = data;
      // Pour ne pas afficher des pages vides (quand on change la limite d'affichage par page nottamment)
      if (state.quizzes.currentPage > state.quizzes.nbPages) {
        state.quizzes.currentPage = state.quizzes.nbPages
        return getQuizzes();
      }
      return data;
    });
};

// Recherche de quizzes
// eslint-disable-next-line no-unused-vars
function search(string) {
  console.debug("@search()");

  const url = `${state.serverUrl}/search/?q=${encodeURI(string)}`;

  return request(url, { method: "GET", headers: state.getHeaders() })
    .then(filterHttpResponse)
    .then((res) => {
      state.searchResult = res;
      return res;
    });
}

// Télécharge les questions d'un quiz d'identifiant 'id'
// eslint-disable-next-line no-unused-vars
function getQuizData(id) {
  console.debug(`@getQuizData(${id})`);

  const url = `${state.serverUrl}/quizzes/${id}/questions`;

  return request(url, { method: "GET", headers: state.getHeaders() }).then(filterHttpResponse);
}

// ////////////////////////////////////////////////////////////////////////////////////////////////
// //////////////////////////////////// Réponses aux quiz /////////////////////////////////////////

// Récupère les réponses de l'utilisateur
// eslint-disable-next-line no-unused-vars
const getUserAnswers = () => {
  console.debug(`@getUserAnswers()`);

  const url = `${state.serverUrl}/users/answers`;

  return request(url, { method: "GET", headers: state.getHeaders() })
    .then(filterHttpResponse)
    .then((data) => {
      const old = state.userQuizzes;
      const obj = {quizzes:  data};

      obj.pageSize = Number(state.settings.limit);
      obj.currentPage = old.currentPage !== undefined? old.currentPage : 1;
      obj.nbResults = data.length;
      obj.nbPages = Math.ceil(obj.nbResults / obj.pageSize);
      obj.localPagination = true;

      state.userAnswers = obj;
      return data;
    });
};

// Envoie la réponse à une question au serveur
// arr est un tableau contenant les identifiants de la question et de la proposition
// eslint-disable-next-line no-unused-vars
const submitQuestion = (arr, id) => {
  console.debug(`@submitQuestion(${id})`);

  const url = `${state.serverUrl}/quizzes/${id}/questions/${arr[0]}/answers/${arr[1]}`;

  return request(url, { method: "POST", headers: state.getHeaders() }).then(filterHttpResponse);
};

// Suppression d'une question
// eslint-disable-next-line no-unused-vars
function deleteAnswer(quizId, questionId) {
  console.debug("@deleteAnswer()");

  const url = `${state.serverUrl}/quizzes/${quizId}/questions/${questionId}/answers`;

  return request(url, { method: "DELETE", headers: state.getHeaders() }).then(filterHttpResponse);
}

// ///////////////////////////////////////////////////////////////////////////////////////////////////////
// //////////////////////////////////////////// Gérer un Quiz ////////////////////////////////////////////

// Création d'un quiz
// 'form' est l'objet FormData contenant les infos du nouveau quiz
// eslint-disable-next-line no-unused-vars
const newQuiz = (form) => {
  console.debug("@newQuiz");

  // Génère l'URL
  const url = `${state.serverUrl}/quizzes`;

  // Génère l'objet à envoyer
  const data = { title: form.get("titre"), description: form.get("desc") };

  // Retourne une promesse qui attend le traitement du résultat de la requête
  return request(url, {
    method: "POST",
    headers: state.getHeaders(),
    body: JSON.stringify(data),
  }); // Réponse filtrée dans un 2 ème temps pour gérer les différents retours plus facilement
};

// Wrapper pour la création d'un quiz et le rendu
// eslint-disable-next-line no-unused-vars
const newQuizUpdate = (form) => newQuiz(form).then(handleNewQuizResponse);

// Modifie les informations d'un quiz
// form est un objet FormData contentant les mmodifications souhaitées
// eslint-disable-next-line no-unused-vars
function updateQuiz(form, id) {
  console.debug(`@updateQuiz(${id})`);

  const url = `${state.serverUrl}/quizzes/${id}`;
  const status = form.get("status"); // ouvert ou fermé
  const t = form.get("titre");
  const desc = form.get("desc");

  const data = { title: t, description: desc, open: status };
  return request(url, {
    method: "PUT",
    headers: state.getHeaders(),
    body: JSON.stringify(data),
  }); // Réponse filtrée dans un 2 ème temps pour gérer les différents retours plus facilement
}

// Wrapper pour ouvrir un quiz au public
// eslint-disable-next-line no-unused-vars
function openQuiz(quiz) {
  const form = new FormData();
  form.append("status", "true");
  form.append("titre", quiz.info.title);
  form.append("desc", quiz.info.description);
  return updateQuiz(form, quiz.info.quiz_id).then(handleQuizUpdateResponse);
}

// Wrapper pour fermer un quiz au public
// eslint-disable-next-line no-unused-vars
function closeQuiz(quiz) {
  const form = new FormData();
  form.append("status", "false");
  form.append("titre", quiz.info.title);
  form.append("desc", quiz.info.description);
  return updateQuiz(form, quiz.info.quiz_id).then(handleQuizUpdateResponse);
}

// Suppression d'un quiz
// eslint-disable-next-line no-unused-vars
const deleteQuiz = (id) => {
  console.debug("@deleteQuiz()");
  // Génère l'url en récupérant l'identifiant du quiz
  const url = `${state.serverUrl}/quizzes/${id}`;

  // Retourne une promesse attendant la réponse du serveur
  return request(url, { method: "DELETE", headers: state.getHeaders() }).then(filterHttpResponse);
};

// ///////////////////////////////////////////////////////////////////////////////////////////////////////
// /////////////////////////////////////// Gérer une question ////////////////////////////////////////////

// Ajoute une question à un quiz
// 'id' est l'identifiant du quiz
// 'form' est l'objet FormData contenant les infos de la nouvelle question
// 'index' est l'index de la nouvelle question dans le quiz
// eslint-disable-next-line no-unused-vars
const addQuestion = (id, form, index) => {
  console.debug("@addQuestion()");

  // Créer un tableau avec les données du formulaire pour un traitement plus facile
  const A = Array.from(form);

  // Récupère le nom du champ contenant la proposition juste
  const answer = form.get("answer");

  // Initialisation du compteur pour les identifiants des propositions
  let count = 0;

  // boom : booléen
  // Si boom = true, la bonne proposition est vide => pas possible
  let boom = false;

  // Crée le tableau des propositions
  const prop = A.reduce(function getPropositions(acc, e) {
    // définition du callback getPropositions :
    // acc = accumulateur, e = élément du tableau

    if (!Number.isNaN(Number(e[0]))) {
      // Si le champ est un nombre => c'est une proposition
      // Correct : bool : proposition est la réponse ??
      const Correct = e[0] === answer;

      if (e[1] !== "") {
        // Si la proposition n'est pas vide
        // Ajoute un objet "proposition" à l'accumulateur
        acc.push({
          content: e[1],
          proposition_id: count,
          correct: Correct,
        });
        // Incrémente le compteur
        count += 1;
      }
      // Sinon (si la proposition est vide) et si la proposition est marquée comme être la réponse :
      else if (Correct) boom = true;
    }
    return acc;
  }, []); // accumulateur initialisé avec un tableau vide

  // Si la réponse est vide : gère cette erreur
  if (boom) return Promise.reject(answer);

  // Génère l'url
  const url = `${state.serverUrl}/quizzes/${id}/questions`;

  // Génère l'objet à envoyer
  const data = {
    question_id: index,
    sentence: form.get("sentence"),
    propositions: prop,
  };

  // Retourne une promesse qui attend la réponse du serveur
  return request(url, {
    method: "POST",
    headers: state.getHeaders(),
    body: JSON.stringify(data),
  }).then(filterHttpResponse);
};

// Wrapper pour ajouter une question et gérer le rendu de la réponse
// eslint-disable-next-line no-unused-vars
const addQuestionUpdate = (id, form, index) =>
  addQuestion(id, form, index).then(renderNewQuestOK).catch(renderNoAnswer);

// Modification d'une question
// Les modifications sont dans le FormData form
// eslint-disable-next-line no-unused-vars
function updateQuestion(form, quizId, questionId) {
  console.debug("@updateQuestion()");

  const answer = form.get("answer"); // récupère le nom du champ de la bonne réponse
  const arr = Array.from(form);

  const prop = arr.reduce((acc, e) => {
    if (e[0] !== "content" && e[0] !== "answer") {
      // Pour tous les champs de type proposition
      const data = {
        content: e[1],
        proposition_id: e[0],
        correct: e[0] === answer,
      };
      acc.push(data);
    }
    return acc;
  }, []);

  // On constitue l'objet à envoyer
  const data = { sentence: form.get("content"), propositions: prop };

  const url = `${state.serverUrl}/quizzes/${quizId}/questions/${questionId}`;
  return request(url, {
    method: "PUT",
    headers: state.getHeaders(),
    body: JSON.stringify(data),
  }).then(filterHttpResponse);
}

// Suppression d'une question
// eslint-disable-next-line no-unused-vars
function deleteQuestion(quizId, questionId) {
  console.debug("@deleteQuestion()");

  const url = `${state.serverUrl}/quizzes/${quizId}/questions/${questionId}`;

  return request(url, { method: "DELETE", headers: state.getHeaders() }).then(filterHttpResponse);
}
