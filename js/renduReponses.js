/* global M htmlMsgErrorNewQuiz htmlMsgNoAnswer closeTooltips getUserQuizzes htmlDeleteAnswerBtn
getUserAnswers refreshQuizzes renderUserBtn deleteAnswer changeFilter state */

// //////////////////////////////////////////////////////////////////////////////
// RENDUS (partie 3): rendus pour les réponses aux requêtes du serveur
// //////////////////////////////////////////////////////////////////////////////

// ////////////////////////////////////////////////////////////////////////////////////////////////
// /////////////////////////////////// Fonctions utilitaires //////////////////////////////////////

// Met un '0' devant un nombre à un seul chiffre
// Renvoie une string
const pad2digits = (n) => {
  return n >= 0 && n < 10 ? `0${n}` : String(n);
};

// Formatte une date selon le format Français
//  d : objet Date
// retour : {date: "dd/mm/yyyy", time: "hh:mm::ss"}
// eslint-disable-next-line no-unused-vars
const parseDate = (d) => {
  return {
    date: `${pad2digits(d.getDate())}/${pad2digits(d.getMonth() + 1)}/${d.getFullYear()}`,
    time: `${pad2digits(d.getHours())}:${pad2digits(d.getMinutes())}:${pad2digits(d.getSeconds())}`,
  };
};

// ////////////////////////////////////////////////////////////////////////////////////////////////
// /////////////// Pour gérer les différentes réponses à certaines requêtes ///////////////////////

// Gère la réponse de getUser
// eslint-disable-next-line no-unused-vars
function handleGetUser(user) {
  // Si l'utilisateur est connecté
  if (user !== undefined) {
    // On enlève l'avertissement en le faisant glisser vers le haut
    document.getElementById("warning-wrapper").style.marginTop = "-4rem";

    // On récupère et affiche les réponses de l'utilisateur puis affiche les quizzes globaux
    // Une fois qu'on a les réponses de l'utilisateur
    Promise.all([getUserAnswers(), getUserQuizzes()])
      .then(() => refreshQuizzes())
      .catch(err => console.error(`ERROR : handleGetUser : ${err}`));
  } else {
    // Sinon l'utilisateur n'est pas connecté

    // On affiche l'avertissement
    document.getElementById("warning-wrapper").style.marginTop = "0px";

    // On vide les listes de quizzes de l'utilisateur et des réponses
    state.userAnswers = [];
    state.userQuizzes = [];

    delete document.getElementById("id-quizzes-main").dataset.changing;

    // On recharge les quizzes
    refreshQuizzes();
  }
  // On lance le rendu du bouton de login
  renderUserBtn();
}

// Gère la réponse du serveur lors de la création d'un quiz
// eslint-disable-next-line no-unused-vars
function handleNewQuizResponse(response) {
  return (
    response
      .json() // parse le json
      .then((data) => {
        if (response.status === 409) {
          // Si retour d'une erreur 409 : le nom donné est déja pris
          return newQuizNotOK(data);
        }
        if (response.status >= 400 && response.status < 600) {
          // Si erreur quelconque on crée une erreur
          throw new Error(`${data.name}: ${data.message}`);
        }
        // Sinon on gère le succès de la création du quiz
        return newQuizOK(data.quiz_id);
      })
      // Si on a une erreur, on l'affiche dans la console
      .catch((err) => console.error(`Error : handleNewQuizResponse : ${err}`))
  );
}

// Gère la réponse du serveur lors de la modification d'un quiz
// eslint-disable-next-line no-unused-vars
function handleQuizUpdateResponse(response) {
  return (
    response
      .json() // parse le json
      .then((data) => {
        if (response.status === 500) {
          // Si retour d'une erreur 500 : le nom donné est déja pris
          return newQuizNotOK(data);
        }
        if (response.status >= 400 && response.status < 600) {
          // Si erreur quelconque on crée une erreur
          throw new Error(`${data.name}: ${data.message}`);
        }
        // Sinon on gère le succès de la création du quiz
        return renderUpdateOK(data);
      })
      // Si on a une erreur, on l'affiche dans la console
      .catch((err) => console.error(`Error : handleQuizUpdateResponse : ${err}`))
  );
}

// On gère l'erreur si le nom du quiz existe déja
// eslint-disable-next-line no-unused-vars
function newQuizNotOK() {
  // On récupère le champ "nom" du formulaire
  const formElt = document.querySelector("#id-quizzes-main input");
  const main = document.getElementById("id-quizzes-main");

  main.dataset.changing = "";

  // On récupère les éléments du modal
  const modal = document.getElementById("id-modal-quizz-menu");
  const redBtn = document.getElementById("redBtn");
  const greenBtn = document.getElementById("greenBtn");

  // On charge son contenu
  modal.children[1].innerHTML = htmlMsgErrorNewQuiz();
  greenBtn.onclick = undefined;
  redBtn.onclick = undefined;

  greenBtn.innerHTML = "Réessayer";
  redBtn.innerHTML = "";
  greenBtn.style.display = "block";
  redBtn.style.display = "none";

  modal.style.width = "17%";

  // On ouvre le modal
  M.Modal.getInstance(modal).open();

  // On marque le champ "nom" du formulaire comme invalide
  formElt.classList.add("invalid");
}

// Gère l'absence de texte dans la réponse sélectionée lors de la création d'une question
// eslint-disable-next-line no-unused-vars
function renderNoAnswer(id) {
  // Récupère les éléments du modal
  const modal = document.getElementById("id-modal-quizz-menu");
  const redBtn = document.getElementById("redBtn");
  const greenBtn = document.getElementById("greenBtn");

  // Charge le contenu du modal
  modal.children[1].innerHTML = htmlMsgNoAnswer();
  greenBtn.onclick = undefined;
  redBtn.onclick = undefined;

  greenBtn.innerHTML = "Réessayer";
  redBtn.innerHTML = "";
  greenBtn.style.display = "block";
  redBtn.style.display = "none";

  modal.style.width = "17%";

  M.Modal.getInstance(modal).open();

  const formElt = document.querySelector(`#newQuest [id='${id}']`);
  formElt.classList.add("invalid");
}

// ////////////////////////////////////////////////////////////////////////////////////////////////
// ///////////////////// Rendu des confirmations visuelles des réponses ///////////////////////////

// Gère la confirmation visuelle de la réponse à une question
// eslint-disable-next-line no-unused-vars
function renderAnswerOK(res) {
  // On update l'affichage des réponses
  const main = document.getElementById("id-quizzes-main");
  // Si le quiz est toujours affiché
  if (Number(main.dataset.id) === res.quiz_id) {
    // Si la réponse n'a pas de bouton pour la supprimer
    if (document.querySelector(`p#sentence-${res.question_id} > a.deleteAnswerBtn`) === null) {
      document.getElementById(`sentence-${res.question_id}`).innerHTML += htmlDeleteAnswerBtn(
        res.question_id
      );
      const btn = document.querySelector(`p#sentence-${res.question_id} > a.deleteAnswerBtn`);
      btn.onclick = () => deleteAnswer(res.quiz_id, res.question_id).then(renderDeleteAnswerOK);
    }
  }

  // Formatte la date et l'heure
  const date = parseDate(new Date(res.answered_at));

  // Affiche une notification
  const toast = M.toast({
    // Génère le code html de la notification
    html: `${res.user_id} a répondu ${res.proposition_id} à  la question ${res.question_id}</br> du quiz ${res.quiz_id} à ${date.time}`,
    classes: "toast-answer",
    displayLength: 5000, // Affichage persiste pendant 5s
  });

  // Quand on clique sur la notification, elle disparaît
  toast.el.onclick = function dismiss() {
    toast.timeRemaining = 0; // Change le temps restant à l'affichage à 0s => disparition
  };

  return res;
}

// Gère la confirmation visuelle de la suppression d'une réponse
// eslint-disable-next-line no-unused-vars
function renderDeleteAnswerOK(res) {
  // Update l'interface
  const input = document.querySelector(
    `#quizz input[type="radio"][name="${res.question_id}"][value="${res.proposition_id}"]`
  );
  input.checked = false;
  document.querySelector(`#quizz a[data-id="${res.question_id}"]`).remove();

  // Affiche une notification
  const toast = M.toast({
    // Génère le code html de la notification
    html: `La réponse à la question ${res.question_id} du quiz d'identifiant ${res.quiz_id} à été supprimée`,
    classes: "toast-delete",
    displayLength: 5000, // Affichage persiste pendant 5s
  });

  // Quand on clique sur la notification, elle disparaît
  toast.el.onclick = function dismiss() {
    toast.timeRemaining = 0; // Change le temps restant à l'affichage à 0s => disparition
  };
}

// ////////////////////////////////////////////////////////////////////////////////////////////////
// ///////////////////// Rendu des confirmations visuelles des quizzes ////////////////////////////

// On gère la bonne soumission du nouveau quiz
// eslint-disable-next-line no-unused-vars
function newQuizOK(id) {
  // Affiche une notification
  const toast = M.toast({
    // Génère le code html de la notification
    html: `Le quiz d'identifiant ${id} à été créé`,
    classes: "toast-create",
    displayLength: 5000, // Affichage persiste pendant 5s
  });

  // Quand on clique sur la notification, elle disparaît
  toast.el.onclick = function dismiss() {
    toast.timeRemaining = 0; // Change le temps restant à l'affichage à 0s => disparition
  };

  // On affiche la liste de quiz de l'utilisateur, et on préselectionne le nouveau quiz

  changeFilter("user");
  document.getElementById("id-quizzes-main").dataset.id = id;
  state.settings.order = "quiz_id";
  state.settings.dir = "desc";
  state.userQuizzes.currentPage = 1;
  state.currentQuiz = {info: {quiz_id: id}};
}

// Gère la confirmation visuelle de la modification d'un quiz
// eslint-disable-next-line no-unused-vars
function renderUpdateOK(res) {
  // Affiche une notification
  const toast = M.toast({
    // Génère le code html de la notification
    html: `Le quiz d'identifiant ${res.quiz_id} à été modifié`,
    classes: "toast-update",
    displayLength: 5000, // Affichage persiste pendant 5s
  });

  // Quand on clique sur la notification, elle disparaît
  toast.el.onclick = function dismiss() {
    toast.timeRemaining = 0; // Change le temps restant à l'affichage à 0s => disparition
  };

  // On ferme les tooltips (qui restent parfois bloqués :-( )
  closeTooltips();
}

// Gère la confirmation visuelle de la suppression d'un quiz
// eslint-disable-next-line no-unused-vars
function renderDeleteOK(res) {
  // Affiche une notification
  const toast = M.toast({
    // Génère le code html de la notification
    html: `Le quiz d'identifiant ${res.quiz_id} à été supprimé`,
    classes: "toast-delete",
    displayLength: 5000, // Affichage persiste pendant 5s
  });

  // Quand on clique sur la notification, elle disparaît
  toast.el.onclick = function dismiss() {
    toast.timeRemaining = 0; // Change le temps restant à l'affichage à 0s => disparition
  };

  return res;
}

// ////////////////////////////////////////////////////////////////////////////////////////////////
// ///////////////////// Rendu des confirmations visuelles des questions ////////////////////////////

// Gère la confirmation visuelle de la création d'une question
// eslint-disable-next-line no-unused-vars
function renderNewQuestOK(res) {
  // Affiche une notification
  const toast = M.toast({
    // Génère le code html de la notification
    html: `La question ${res.question_id} du quiz d'identifiant ${res.quiz_id} <br/> a été créée`,
    classes: "toast-create",
    displayLength: 5000, // Affichage persiste pendant 5s
  });

  // Quand on clique sur la notification, elle disparaît
  toast.el.onclick = function dismiss() {
    toast.timeRemaining = 0; // Change le temps restant à l'affichage à 0s => disparition
  };

  return res;
}

// Gère la notification lors de la modification d'une question
// eslint-disable-next-line no-unused-vars
function renderUpdateQuestOK(res) {
  // Affiche une notification
  const toast = M.toast({
    // Génère le code html de la notification
    html: `La question ${res.question_id} du quiz d'identifiant ${res.quiz_id} à été modifiée`,
    classes: "toast-update",
    displayLength: 5000, // Affichage persiste pendant 5s
  });

  // Quand on clique sur la notification, elle disparaît
  toast.el.onclick = function dismiss() {
    toast.timeRemaining = 0; // Change le temps restant à l'affichage à 0s => disparition
  };

  // On ferme les tooltips (qui restent parfois bloqués :-( )
  closeTooltips();
}

// Gère la confirmation visuelle de la suppression d'une question
// eslint-disable-next-line no-unused-vars
function renderDeleteQuestOK(res) {
  // Affiche une notification
  const toast = M.toast({
    // Génère le code html de la notification
    html: `La question ${res.question_id} du quiz d'identifiant ${res.quiz_id} à été supprimée`,
    classes: "toast-delete",
    displayLength: 5000, // Affichage persiste pendant 5s
  });

  // Quand on clique sur la notification, elle disparaît
  toast.el.onclick = function dismiss() {
    toast.timeRemaining = 0; // Change le temps restant à l'affichage à 0s => disparition
  };

  return res;
}
