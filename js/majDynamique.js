/* global refreshQuizzes renderQuizzes state getQuizzes getUserAnswers getUserQuizzes */

// //////////////////////////////////////////////////////////////////////////////
// RAFFRAICHISSEMENT : mise en place du rafraichissement dynamique de la page
// //////////////////////////////////////////////////////////////////////////////

// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ///////////////////////////////////// Rafraichissement dynamique de la page //////////////////////////////////////

// Rafraichissement lors de la modification d'une réponse
function refreshAnswer(msg) {
  if (state.user !== undefined)
    // Si l'utilisateur est connecté, on recharge ses réponses, puis
    getUserAnswers()
      .then(() => {
        // Si le quiz est affiché avec le filtre "réponses"
        // Mais qu'il n'est plus dans la listes de quiz répondus,
        // On refait le rendu des quiz pour l'enlever de la liste
        if (
          msg.operation === "DELETE" &&
          state.selection === "answers" &&
          state.userAnswers.quizzes !== undefined && 
          state.userAnswers.quizzes.find((e) => e.quiz_id === msg.quizId) === undefined
        )
          return renderQuizzes();

        // Sinon on ne fait rien
        return undefined;
      })
      .catch((err) => console.error(`ERROR : refreshAnswer : ${err}`));
}

// Rafraichissement lors de la modification d'une question
async function refreshQuest(msg) {
  if (msg.operation === "UPDATE") {
    // Si le quiz modifié est affiché, on recharge son affichage
    if (state.currentQuiz !== undefined && state.currentQuiz.info.quiz_id === msg.quizId)
    renderQuizzes();
  } else {
    // Si on ajoute ou retire une question
    // On recharge les listes globales et de l'utilisateur quand le quiz modifié y figure
    //    => nécessaire car le nombre de questions dans le quiz à été changé (champ questions_number)

    const toFetch = [];
    if (state.userQuizzes.quizzes !== undefined && state.userQuizzes.quizzes.find((e) => e.quiz_id === msg.quizId)) {
      toFetch.push(getUserQuizzes());
    }

    if (state.quizzes.results !== undefined && state.quizzes.results.find((e) => e.quiz_id === msg.quizId)) {
      toFetch.push(getQuizzes());
    }

    // Puis on recharge l'affichage
    Promise.all(toFetch)
      .then(() => renderQuizzes())
      .catch((err) => console.error(`ERROR : refreshQuest : ${err}`));
  }
}

// Rafraichissement lors de la modification d'un quiz
async function refreshQuizInfo(msg) {
  switch (msg.operation) {
    case "DELETE":
      // Si le quiz affiché est dans la liste affichée
      if (
        Array.from(document.querySelectorAll("#id-quizzes-list li")).find(
          (e) => Number(e.dataset.quizzid) === msg.quizId
        ) !== undefined
      ) {

        // On recharge la liste de quizzes globale
        const toFetch = [getQuizzes()];
        // Et celle de l'utilisateur si l'utilisateur est connecté
        if (state.user !== undefined) toFetch.push(getUserQuizzes());

        Promise.all(toFetch)
          // Puis on recharge l'affichage
          .then(() => renderQuizzes())
          .catch((err) => console.error(`ERROR : refreshQuizInfo (DELETE) : ${err}`));
      }
      break;

    case "UPDATE": {
      const toFetch = [];

      // On récupère les données de l'utilisateur si besoin
      if (
        state.userQuizzes.quizzes !== undefined &&
        state.userQuizzes.quizzes.find((e) => e.quiz_id === msg.quizId) !== undefined
      ) {
        toFetch.push(getUserQuizzes());
      }
      // Puis celles de la liste globale
      if (state.quizzes.results !== undefined && state.quizzes.results.find((e) => e.quiz_id === msg.quizId) !== undefined) {
        toFetch.push(getQuizzes());
      }
      // Puis celles de la liste des réponses
      if (state.userAnswers.quizzes !== undefined && state.userAnswers.quizzes.find((e) => e.quiz_id === msg.quizId) !== undefined) {
        toFetch.push(getUserAnswers());
      }

      const li = Array.from(document.querySelectorAll("#id-quizzes-list > ul > li"));

      // On recharge l'affichage si on a changé des informations ou si on affiche le quiz dans la liste
      if (
        toFetch.length > 0 ||
        li.find((e) => Number(e.dataset.quizzid) === msg.quizId) !== undefined
      )
        Promise.all(toFetch)
          .then(() => renderQuizzes())
          .catch((err) => console.error(`ERROR : refreshQuizInfo (UPDATE) : ${err}`));
      break;
    }

    case "INSERT": {
      // On récupère les données de l'utilisateur si besoin
      if (state.user !== undefined) await getUserQuizzes();
      // Puis la liste globale
      refreshQuizzes();
      break;
    }
    default:
  }
}

// Callback pour la mise à jour dynamique
// eslint-disable-next-line no-unused-vars
function refreshAll(msg) {
  if (msg.type !== "info") {
    console.debug("@refreshAll()");
    console.info(msg);

    switch (msg.type) {
      case "answer":
        return refreshAnswer(msg);

      case "question":
      case "proposition":
        return refreshQuest(msg);

      case "quiz":
        return refreshQuizInfo(msg);

      default:
        console.error(`Error on type in refreshAll ${msg.type}`);
        return undefined;
    }
  }
  // Si message de type "info" (au lancement du socket)
  return undefined;
}

// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ///////////////////////////// Construction du callback pour la mise à jour dynamique /////////////////////////////

// transformer le "scope" du changement en nombre
function scope2nb(scope) {
  switch (scope) {
    case "quiz":
      return 3;

    case "question":
      return 2;

    case "proposition":
      return 1;

    case "answer":
      return 0;

    default:
      console.error("Error : scope2nb");
      return undefined;
  }
}

// Fait correspondre un nombre au "scope" du changement associé
function nb2scope(n) {
  switch (n) {
    case 0:
      return "answer";

    case 1:
      return "proposition";

    case 2:
      return "question";

    case 3:
      return "quiz";

    default:
      console.error("Error : nb2scope");
      return undefined;
  }
}

// Renvoi du scope de changement le plus important
function getHigherScope(s1, s2) {
  const maxScopeValue = Math.max(scope2nb(s1), scope2nb(s2));
  return nb2scope(maxScopeValue);
}

// Décoration du callback pour la misa à jour dynamique
// Pour ne pas avoir des mises à jour successives intiles
// Fonctionnement : On attend un temps défini et on considère les changements les plus importants
// eslint-disable-next-line no-unused-vars
function callbkTreatment(callback) {
  // ------ variables ----------
  const delta = 500; // (en ms) durée d'attente d'une série de messages
  let firstmsgTime;
  let serieQuizId;
  let serieOp;
  let highestScope;
  let data = {};

  return function newFunc(msg) {
    const msgObj = JSON.parse(msg);

    const time = new Date(msgObj.time).getTime();
    const diff = firstmsgTime === undefined ? undefined : time - firstmsgTime;
    const quizId = msgObj.quiz_id;
    const op = msgObj.operation;
    const scope = msgObj.type;

    if (diff === undefined || diff > delta || quizId !== serieQuizId || op !== serieOp) {
      // Si on a une nouvelle "série" de messages
      // On initialise les variables pour traquer la série
      firstmsgTime = time;
      serieQuizId = quizId;
      serieOp = op;
      highestScope = scope;

      // On construit l'objet pour traiter le rafraichissement
      data = {
        type: highestScope,
        operation: serieOp,
        quizId: serieQuizId,
        time: firstmsgTime,
      };

      setTimeout(callback, delta, data);
    } else {
      // Si on est dans une "série"

      data.type = getHigherScope(data.type, scope);
    }
  };
}

// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ///////////////////////////////// Installation du Websocket //////////////////////////////////////////////////////

// Cette fonction permet d'installer un websocket natif navigateur en écoute
// https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API
// A chaque message recu, le callback sera appellé avec son contenu
// eslint-disable-next-line no-unused-vars
function installWebSocket(callbackOnMessage) {
  // Server's address
  const socket = new WebSocket(state.getStreamUrl());

  console.debug(`installWebSocket@`);
  // the global heartbeat's ID
  let heartbeatInterval;

  // Connection opened
  socket.onopen = (event) => {
    // Set some heartbeat to the server. WebSocket native API does not support ping yet.
    // Nginx's timeout is 60s, so we take the half
    console.debug(`socket.onopen@${JSON.stringify(event)}`);
    heartbeatInterval = setInterval(() => {
      const heartbeat = { type: "heartbeat", time: Date.now() };
      // console.info(`Sent: ${JSON.stringify(heartbeat)}`);
      socket.send(JSON.stringify(heartbeat));
    }, 30000);
  };

  // Connection closed
  socket.onclose = (event) => {
    clearInterval(heartbeatInterval);
    console.debug(`socket.onclose@${event.code}`);
  };

  // Listen for messages
  socket.onmessage = (event) => {
    // console.info(`Received: ${event.data}`);
    /*  event.data on different actions
    answer
      {"type":"answer","operation":"UPDATE","quiz_id":7376,"time":"2020-05-10T11:01:24.869323+02:00"}
    creation
      {"type":"quiz","operation":"INSERT","quiz_id":5111,"time":"2020-05-03T13:15:49.947435+02:00"} 
    update
      {"type":"quiz","operation":"UPDATE","quiz_id":5111,"time":"2020-05-03T13:16:28.753556+02:00"} 
    new quest
      {"type":"question","operation":"INSERT","quiz_id":5111,"time":"2020-05-03T13:17:23.667655+02:00"} 
      {"type":"proposition","operation":"INSERT","quiz_id":5111,"time":"2020-05-03T13:17:23.674202+02:00"} => times nb of propositions
    update quest
      {"type":"question","operation":"UPDATE","quiz_id":5111,"time":"2020-05-03T13:17:55.995663+02:00"} 
      {"type":"proposition","operation":"UPDATE","quiz_id":5111,"time":"2020-05-03T13:17:56.000029+02:00"} => times nb of propositions
    delete
      {"type":"quiz","operation":"DELETE","quiz_id":5111,"time":"2020-05-03T13:19:09.366726+02:00"} 
      {"type":"question","operation":"DELETE","quiz_id":5111,"time":"2020-05-03T13:19:09.366726+02:00"} => once
      {"type":"proposition","operation":"DELETE","quiz_id":5111,"time":"2020-05-03T13:19:09.366726+02:00"} => once
    */
    console.info(event.data);

    callbackOnMessage(event.data);
  };

  window.addEventListener("beforeunload", () => {
    // here a 1005 code is sent
    clearInterval(heartbeatInterval);
    socket.close();
  });

  function sendMessage(msg) {
    if (socket.readyState === WebSocket.OPEN) {
      console.info(`sendMessage@${JSON.stringify(msg)}`);
      // See https://developer.mozilla.org/en-US/docs/Web/API/WebSocket/send
      socket.send(JSON.stringify(msg));
    } else console.error(`sendMessage@state is ${socket.readyState}`);
  }

  return sendMessage;
}
