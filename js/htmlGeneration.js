/* eslint-disable no-nested-ternary */
/* global state */

// //////////////////////////////////////////////////////////////////////////////
// HTML : fonctions génération de HTML à partir des données passées en paramètre
// //////////////////////////////////////////////////////////////////////////////

// //////////////////////////////////////////////////////////////////////////////
// //////////////////////// Fonctions utilitaires ///////////////////////////////

// Pour échapper les caractères pouvant poser problème au html dans une chaine de charactères
function escapeString(string) {
  return Array.from(string)
    .map((char) => {
      // code utf-16 du charactère
      const code = char.codePointAt(); // charCodeAt();
      // Si le charactère est une lettre de l'alphabet ou un chiffre
      if ((code >= 65 && code <= 90) || (code >= 97 && code <= 122) || (code >= 48 && code < 57))
        return char;

      // sinon (charactère spécial) : html code pour le charactère
      return `&#${code};`;
    })
    .join("");
}

// //////////////////////////////////////////////////////////////////////////////
// //////////////////////// Génération de la pagination /////////////////////////

// Génération d'un élément de pagination
const htmlPaginationElt = (page, active = false) =>
  `<li class="waves-effect ${
    active ? "active teal" : ""
  }"><a href="#!" class="pagination-elt" data-page="${page}">${page}</a></li>`;

// Génération de la pagination complète
const htmlPagination = (curr, total) => {
  if (total > 0)
    return `
  <div id="id-pagination">
    <ul class="pagination" id="id-page-left">
      <li class="${
        curr === 1 ? "disabled" : "waves-effect"
      }"><a href="#!" class="pagination-elt" data-page="1"><i class="material-icons">first_page</i></a></li>
      <li class="${
        curr === 1 ? "disabled" : "waves-effect"
      }"><a href="#!" class="pagination-elt" data-page="${
      curr - 1
    }"><i class="material-icons">chevron_left</i></a></li>
    </ul>
    <ul class="pagination" id="id-page-center">
     ${curr > 3 ? htmlPaginationElt(curr - 3) : ""}
     ${curr > 2 ? htmlPaginationElt(curr - 2) : ""}
     ${curr > 1 ? htmlPaginationElt(curr - 1) : ""}
     ${htmlPaginationElt(curr, true)}
     ${curr + 1 <= total ? htmlPaginationElt(curr + 1) : ""}
     ${curr + 2 <= total ? htmlPaginationElt(curr + 2) : ""}
     ${curr + 3 <= total ? htmlPaginationElt(curr + 3) : ""}
    </ul>
    <ul class="pagination" id="id-page-right">
      <li class="${
        curr === total ? "disabled" : "waves-effect"
      }"><a href="#!" class="pagination-elt" data-page="${
      curr + 1
    }"><i class="material-icons">chevron_right</i></a></li>
      <li class="${
        curr === total ? "disabled" : "waves-effect"
      }"><a href="#!" class="pagination-elt" data-page="${total}"><i class="material-icons">last_page</i></a></li>
    </ul>
  </div>
  `;
  // Si on n'a rien à afficher (0 pages au total)
  return '';
};

// //////////////////////////////////////////////////////////////////////////////
// //////////////////////// Génération des listes de quiz ///////////////////////

// Génère le code html des éléments de liste
// 'quzzesList' est un tableau d'informations sur des quiz
// Retourne un tableau contenant le code html de chaque élément de la liste
const htmlListElt = (quizzesList) =>
  quizzesList.map(
    (qu) =>
      `<li class="shadow collection-item cyan lighten-5 listElt truncate" data-quizzid="${
        qu.quiz_id
      }"> 
        ${
          state.user !== undefined && state.user.user_id === qu.owner_id // Si l'utilisateur est propriétaire du quiz
            ? '<span class="delete">&times;</span>' // Affiche la croix permettant la suppression du quiz
            : "" // Sinon on n'affiche rien en plus
        }
        <h5>${escapeString(qu.title)}<a class="chip">${qu.owner_id}</a></h5>
         ${escapeString(qu.description)} 
      </li>`
  );

// Génération d'une liste de quizzes avec la pagination
// eslint-disable-next-line no-unused-vars
function htmlQuizzesListGlobal(quizzes) {
  // un élement <li></li> pour chaque quiz
  const quizzesLIst = quizzes.results !== undefined? htmlListElt(quizzes.results) : [];

  const html = `
  <ul class="collection">
    ${quizzesLIst.join("")}
  </ul>
  ${htmlPagination(quizzes.currentPage, quizzes.nbPages)}
  `;
  return html;
}

// ////////////////////////////////////////////////////////////////////////////////////////////////
// //////////////// Génération du code html des boutons de filtrage des quiz //////////////////////

// Génération d'un bouton pour filter les quizzes
// eslint-disable-next-line no-unused-vars
function htmlFilterBtn(text, id, value, selection) {
  return `
  <div class="col s4">
    <label>
      <input type="radio" name="filter" id="${id}" value="${value}" ${
    selection === value ? "checked" : ""
  }><span>${escapeString(text)}</span>
    </label>
  </div>`;
}

// Génération de la partie pour filter les quizzes
// eslint-disable-next-line no-unused-vars
function htmlQuizFilter(selection) {
  return `<div class="row">
  <div class="col s4">
    <p> Filter les quizzes </p>
    ${htmlFilterBtn("Tous les quizzes", "globalFilter", "global", selection)}
    ${htmlFilterBtn("Mes quiz", "userFilter", "user", selection)}
    ${htmlFilterBtn("Mes réponses", "answersFilter", "answers", selection)}
    </div>
  </div>`;
}

// ////////////////////////////////////////////////////////////////////////////////////////////////
// ////////////////// Génération du code html des boutons de triage des quiz //////////////////////

// Génère le formulaire de tri des quiz
// eslint-disable-next-line no-unused-vars
function htmlQuizSort() {
  return `
  <div class="input-field col s4">
    <select id="limit">
      <option value="2">2</option>
      <option value="10">10</option>
      <option value="25">25</option>
      <option value="50">50</option>
      <option value="100">100</option>
      <option value="150">150</option>
      <option value="200">200</option>
    </select>
    <label>Affichage par page</label>
  </div>
  <div class="input-field col s4">
    <select id="order">
      <option value="quiz_id">ID</option>
      <option value="created_at">Date de création</option>
      <option value="title">Titre</option>
      <option value="owner_id">Auteur</option>
    </select>
    <label>Trier par</label>
  </div>
  <div class="input-field col s4">
    <select id="dir">
      <option value="asc">Ascendant</option>
      <option value="desc">Descendant</option>
    </select>
    <label>Ordre</label>
  </div>`;
}

// ////////////////////////////////////////////////////////////////////////////////////////////////
// ///////////////// Génération du code html des boutons utilitaires //////////////////////////////

// On génère le html du bouton d'update du quiz
const htmlUpdateBtn = () =>
  `<br/><a class="btn btn-floating orange darken-1 tooltipped" id="updateBtn" data-position="right" data-tooltip="Modifier les informations du quiz"> 
    <i class="material-icons">mode_edit</i>
  </a>`;

// On génère le bouton d'ouverture du quiz
const htmlOpenBtn = () =>
  `<a class="btn btn-floating red lighten-1 tooltipped" id="openBtn" data-position="right" data-tooltip="Rendre le quiz public"> 
    <i class="material-icons">vpn_lock</i>
  </a>`;

// On génère le bouton de fermeture du quiz
const htmlCloseBtn = () =>
  `<a class="btn btn-floating green darken-1 tooltipped" id="closeBtn" data-position="right" data-tooltip="Fermer le quiz au public"> 
    <i class="material-icons">public</i>
  </a>`;

// On génère le html du bouton d'ajout de question au quiz
const htmlAddQuestionBtn = () =>
  `<br/><a class="btn-floating green lighten-1 tooltipped" data-tooltip="Ajouter une question" data-position="right" id="addQst"> 
    <i class="material-icons">add_circle</i>
  </a>`;

// On génère le bouton d'ajout de quiz
const htmlAddBtn = () => `
  <div class="fixed-action-btn">
  <a class="tooltipped btn-floating btn-large green darken-2" data-position="top" data-tooltip="Créer un nouveau Quiz" id="add">
    <i class="large material-icons">add</i>
  </a>
  </div>
`;

// On génère le bouton d'annulation d'ajout de quiz
const htmlCancelAddBtn = () => `
  <div class="fixed-action-btn">
  <a class="tooltipped btn-floating btn-large red" data-position="top" data-tooltip="Annuler" id="add-cancel">
    <i class="large material-icons">clear</i>
  </a>
  </div>
`;

// On génère les boutons de modification d'un quiz
const htmlUserBtns = (quiz) => `
<div class="col s1">
  ${quiz.info.open ? htmlCloseBtn() : htmlOpenBtn()}
  ${htmlUpdateBtn()}
  ${htmlAddQuestionBtn()}
  </div>
</div>
${htmlAddBtn()}
`;

// On génère un bouton de modification de question
const htmlUpdateQuestBtn = (id) =>
  `<a class="btn btn-floating btn-small orange darken-1 tooltipped updateQuestBtn" style="float:right" data-id="${id}" data-position="right" data-tooltip="Modifier la question"> 
    <i class="material-icons">mode_edit</i>
  </a>`;

// On génère un bouton de suppression de question
const htmlDeleteQuestBtn = (id) =>
  `<a class="btn btn-floating btn-small red tooltipped deleteQuestBtn" style="float:right; margin-right: 1rem" data-id="${id}" data-position="top" data-tooltip="Supprimer la question"> 
    <i class="material-icons">delete</i>
  </a>`;

// On génère un bouton de suppresison de réponse
const htmlDeleteAnswerBtn = (id) =>
  `<a class="btn btn-floating btn-small red tooltipped deleteAnswerBtn" data-id="${id}" data-position="top" data-tooltip="Supprimer la réponse"> 
  <i class="material-icons">delete_forever</i>
</a>`;

// On génère les boutons d'édition d'une question
const getQuestBtns = (id) =>
  `${htmlUpdateQuestBtn(id)}
  ${htmlDeleteQuestBtn(id)}
`;

// //////////////////////////////////////////////////////////////////////////////
// //////////////////////// Génération des propositions /////////////////////////

// Génère des propositions pour une question
// 'prop' tableau contenant les propositions d'une question
// 'id' identifiant de la question
// 'answer' réponses au quiz déja données par l'utilisateur
// 'disabled' booléen qui indique si les propositions doivent être désactivées
// Retourne un tableau avec le code html de chaque proposition
const htmlPropositions = (prop, id, answer, disabled) =>
  prop.map(
    (p) =>
      `<label>
        <input type="radio" name="${id}" value="${p.proposition_id}" ${
        disabled ? "disabled" : ""
      } ${
        answer !== undefined && answer.proposition_id === p.proposition_id
          ? "checked" // Si une réponse à la question existe et correspond à la proposition actuelle,
          : "" // On ajoute l'attribut "checked"
      }>
        <span>${escapeString(p.content)}</span>
      </label>`
  );

// Génère le code html d'une seule proposition pour pouvoir la modifier
const htmlPropositionUpdate = (proposition, correctionP) => {
  return `
      <div class="row">
        <div class="col offset-s1 s1">
          <label>
            <input name="answer" type="radio"  required value="${proposition.proposition_id}" ${
    correctionP.correct ? "checked" : ""
  }/>
            <span></span>
          </label>
        </div>
        <div class="input-field col s8">
          <input type="text" name="${proposition.proposition_id}" id="${
    proposition.proposition_id
  }" value="${escapeString(proposition.content)}" required class="validate">
          <label class="active" for="${proposition.proposition_id}">Proposition</label>
        </div>
      </div>
    `;
};

// //////////////////////////////////////////////////////////////////////////////
// //////////////////////// Génération des questions ////////////////////////////

// Génère le code html d'une seule question d'un quiz
const htmlSingleQuestion = (question, answer, disabled, owner) => {
  return `<p id="sentence-${question.question_id}">${escapeString(question.sentence)} 
             ${owner ? getQuestBtns(question.question_id) : ""}
             ${answer !== undefined  && !disabled? htmlDeleteAnswerBtn(question.question_id) : ""}
          </p>
          ${htmlPropositions(question.propositions, question.question_id, answer, disabled).join(
            ""
          )}
          `;
};

// Génère le code html d'une seule question en mode "update" => formulaire éditable
const htmlSingleQuestionUpdate = (question, correctionQ) => {
  return `
  <form id="updateQuestForm">
  <div class="input-field">
    <input type="text" id="content"  name="content" value="${escapeString(
      question.sentence
    )}" class="validate" required >
    <label for="content" class="active">Question</label>
  </div>
  ${question.propositions
    .map((p) => {
      const correctionP = correctionQ.propositions.find(
        (e) => e.proposition_id === p.proposition_id
      );
      return htmlPropositionUpdate(p, correctionP);
    })
    .join("")}
  <input type="submit" value="Modifier" class="btn" >
  <a class="btn-floating tooltipped red btn-small" data-tooltip="Annuler" data-position="right" id="cancel" style="margin-left:3rem"> 
    <i class=material-icons>clear</i>
  </a>
</form>`;
};

// Génère le code html des questions d'un quiz
// 'question' tableau contenant les questions d'un quiz
// 'answers' réponses au quiz déja données par l'utilisateur
// 'disabled' booléen qui indique si les propositions des questions doivent être désactivées
// 'owner' booléen indiquant si l'utilisateur est propriétaire du quiz
// 'correctionQ' correction de la question
// Retourne un tableau avec le code html de chaque question
const htmlQuestions = (questions, answers, disabled, owner, correctionQ) =>
  questions.map((q) =>
    correctionQ !== undefined && correctionQ.question_id === q.question_id
      ? // Si on a la correction à la question actuelle (et donc)
        // On veut modifier la question d'identifiant id
        htmlSingleQuestionUpdate(q, correctionQ)
      : // Sinon on affiche la question comme d'habitude
        htmlSingleQuestion(
          q,
          answers !== undefined
            ? answers.find((elt) => elt.question_id === q.question_id)
            : undefined,
          disabled,
          owner
        )
  );

// //////////////////////////////////////////////////////////////////////////////
// //////////////////////// Génération des quiz complets ////////////////////////

// Génère le formulaire pour répondre à un quiz
// 'quiz' données du quiz
// 'answers' réponses déja données
// Retourne le code html du formulaire
function htmlQuiz(quiz, answers) {
  // Est-ce que l'utiliateur est propriétaire du quiz ??
  const userOwns = state.user !== undefined && state.user.user_id === quiz.info.owner_id;

  // Si le quiz est fermé ou appartient à l'utilisateur
  // On empêche les réponses au quiz
  const disabled = !quiz.info.open || userOwns || state.user === undefined;

  // On affiche le bouton submit si le quiz est fermé et n'appartient pas à l'utilisateur
  const submit = !quiz.info.open && !userOwns;

  return `<form id="quizz">
            ${htmlQuestions(quiz.questions, answers, disabled, userOwns).join("<br>")}
            ${
              submit
                ? `</br><input type="submit" value="Quiz fermé" class="btn" id="btn-submit" disabled>`
                : "" // On un bouton submit simplement pour signifier que le quiz est fermé
            }   
            </form>`;
}

// Génère le code html du quiz complet, avec l'entête
// eslint-disable-next-line no-unused-vars
const htmlFullQuiz = (quizz, answers) =>
  `<h5>${escapeString(quizz.info.title)} - ${quizz.info.quiz_id}</h5>
   <p>${escapeString(quizz.info.description)}</p> 
   <p>Crée par ${quizz.info.owner_id}</p>
   ${htmlQuiz(quizz, answers)}`;

// Génère le html complet d'un quiz utilisateur, avec les boutons d'options
// eslint-disable-next-line no-unused-vars
const htmlFullUserQuiz = (quiz) => `
<div class="row">
  <div class="col s8" id="quiz-container">
    ${htmlFullQuiz(quiz)}
  </div>
  ${htmlUserBtns(quiz)}
`;

// /////////////////////////////////////////////////////////////////////////////////////
// //////////////////////// Génération de l'affichage de login /////////////////////////

// Génère le formulaire de connexion de l'utilisateur
// eslint-disable-next-line no-unused-vars
const htmlLogInForm = () => `
<h5>Vous n'êtes pas connecté !</h5>
<form action="#">
  <div class="input-field">
    <i class="material-icons prefix">vpn_key</i>
    <input id="apiKey" type="text" class="validate">
    <label for="apiKey">xApiKey</label>
  </div>
<form>
`;

// Génère le html de l'affichage des informations de l'utilisateur
// eslint-disable-next-line no-unused-vars
const htmlUserInfo = (user) => `
<h5 class="center-align">Utilisateur</h5>
<p class="center-align">${escapeString(user.firstname)} ${escapeString(user.lastname)}</p>
`;

// /////////////////////////////////////////////////////////////////////////////////////
// ////////////////// Génération du code html pour la manipulation de quiz /////////////

// Génération du formulaire de création de quiz
// eslint-disable-next-line no-unused-vars
const htmlNewQuizForm = () => `
<h4>Nouveau Quiz</h4>
<form action="#" id="newQuiz">
  <div class="input-field">
    <input id="titre" name="titre" type="text" class="validate" required>
    <label for="titre">Nom du Quiz</label>
  </div>
  <div class="input-field">
    <textarea id="desc" name="desc" class="materialize-textarea validate" required></textarea>
    <label for="desc">Description du Quiz</label>
  </div>
  <input type="submit" class="btn" value="Créer">
</form>
${htmlCancelAddBtn()}
`;

// Génération du code html du formulaire de modification du quiz
const htmlUpdateForm = (quiz) => {
  return `
  <form id="updateForm">
    <div class="input-field">
      <input id="titre" name="titre" type="text" class="validate" required>
      <label class="active" for="titre">Titre</label>
    </div>
    <div class="input-field">
      <textarea id="desc" name="desc" class="materialize-textarea" required></textarea>
      <label class="active" for="desc">Description</label>
    </div>
    <input type="submit" value="Modifier" class="btn">
  </form>
  <p>Crée par ${quiz.info.owner_id}</p>
  `;
};

// Génération du code html pour la modification du quiz
// eslint-disable-next-line no-unused-vars
const htmlUpdateQuiz = (quiz) => {
  return `<div class="row">
  <div class="col s8" id="quiz-container">
    ${htmlUpdateForm(quiz)}
    ${htmlQuiz(quiz)}
  </div>
  ${htmlUserBtns(quiz)}
`;
};

// Génération du message affiché quand le nom du quiz est déja pris
// eslint-disable-next-line no-unused-vars
const htmlMsgErrorNewQuiz = () => `<h5 style="text-align: center">Ce nom est déja pris !</h5>`;

// /////////////////////////////////////////////////////////////////////////////////////
// ///////////// Génération du code html pour la manipulation de questions /////////////

// Génération du formulaire de création de question
// eslint-disable-next-line no-unused-vars
const htmlNewQuestForm = () => `
<form id="newQuest">
  <div class="input-field">
    <input id="content" name="sentence" type="text" class="validate" required>
    <label for="content">Question</label>
  </div>
  <div class="row">
    <p class="col s1 offset-s1 input-field" id="answerSelection">
      <label>
        <input name="answer" type="radio" value="0" tabindex="-1" checked required/>
        <span></span>
      </label>
    </p>
    <div class="input-field col s9" id="propField">
      <input id="0" name="0" type="text" class="validate" required>
      <label for="0">Proposition</label>
    </div>
    <a class="btn col s1 prop" id="btnProp"><i class="material-icons">textsms</i></a>
  </div>
  <input type="submit" class="btn" value="Ajouter" id="newQuestSubmit">
</form>
`;

// Affiche les questions d'un quiz dont on veut modifier une question
// 'quiz' données du quiz
// 'answers' réponses déja données
// Retourne le code html du formulaire
function htmlUpdateQuestQuiz(quiz, correctionQ) {
  return `<div id="quizz" data-id="${quiz.info.quiz_id}">
            ${htmlQuestions(
              quiz.questions,
              undefined, // On n'a pas répondu au quiz
              true, // On désactive les réponses aux proposition
              true, // L'utilisateur est propriétaire
              correctionQ // Bonne réponse à la question à modifier
            ).join("<br>")}   
            </div>`;
}

// Génération du html pour la modification d'une question
// eslint-disable-next-line no-unused-vars
const htmlUpdateQuest = (quiz, correctionQ) => {
  return `
  <div class="row">
    <div class="col s8" id="quiz-container">
      <h5>${escapeString(quiz.info.title)} - ${quiz.info.quiz_id}</h5>
      <p>${escapeString(quiz.info.description)}</p> 
      <p>Crée par ${quiz.info.owner_id}</p>
      ${htmlUpdateQuestQuiz(quiz, correctionQ)}
    </div>
    ${htmlUserBtns(quiz)}
  `;
};

// Génération du message affiché lorsque une question crée n'a pas de réponse
// eslint-disable-next-line no-unused-vars
const htmlMsgNoAnswer = () => `<h5 style="text-align: center">Il n'y a pas de bonne réponse !</h5>`;
