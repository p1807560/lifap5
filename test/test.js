/* eslint-disable */
const apiKey = prompt("Entrez votre clé api svp");

suite("Test de connexion / Déconnexion", function () {
  test("On teste la connexion (getUser)", async function () {
    state.xApiKey = apiKey;
    await getUser();

    chai.assert.isDefined(state.user);
  });
  test("On teste la déconnexion (getUser)", async function () {
    state.xApiKey = "";
    await getUser();

    chai.assert.isUndefined(state.user);
  });
});

suite("Test de récupération des infos générales des quizzes", function () {
  test("On teste getQuizzes", async function () {
    const res = await getQuizzes(1);

    chai.assert.isDefined(res);
    chai.assert.isArray(res.results);
  });

  test("On tests getUserQuizzes", async function () {
    state.xApiKey = apiKey;
    const res = await getUserQuizzes();

    chai.assert.isDefined(res);
    chai.assert.isArray(res);
    chai.assert.isAtLeast(res.length, 2);
  });

  test("On teste getUserAnswers", async function () {
    state.xApiKey = apiKey;
    const res = await getUserAnswers();
    // console.log(res);

    chai.assert.isDefined(res);
    chai.assert.isArray(res);
  });
});

suite("Test de récupération des questions des quizzes", function () {
  test("On teste getQuizzData", async function () {
    const quizzes = await getQuizzes(1);
    const id = quizzes.results[0].quiz_id;

    const res = await getQuizData(id);

    chai.assert.isDefined(res);
    chai.assert.isArray(res);
    if (res.length > 0) res.map((q) => chai.assert.strictEqual(q.quiz_id, id));
  });
});

suite("Test du soumission de réponse", function () {
  let Id;
  let arr;
  test("On teste submitQuestion", async function () {
    // On connecte l'utilisateur
    state.xApiKey = apiKey;
    await getUser();

    // On récumère les quizzes
    const quizzes = await getQuizzes(1);

    // On cherche un quiz ouvert
    const quiz = quizzes.results.find(e => e.open === true && e.questions_number > 0);
     Id = quiz.quiz_id;

    // On crée un objet formdata
    // ici on répond 1 à la question 0
    arr = [0, 1];
    // On envoie le formulaire
    const res = await submitQuestion(arr, Id);

    // On vérifie qu'on a le bon retour du serveur

    chai.assert.strictEqual(res.quiz_id, Id);
    chai.assert.strictEqual(res.proposition_id, arr[1]);
    chai.assert.strictEqual(res.question_id, arr[0]);
    chai.assert.strictEqual(res.user_id, state.user.user_id);
  });

  test("On teste deleteAnswer", async function () {
    const res = await deleteAnswer(Id, arr[0]);

    chai.assert.isDefined(res);
    chai.assert.strictEqual(res.quiz_id, Id);
    chai.assert.strictEqual(res.proposition_id, arr[1]);
    chai.assert.strictEqual(res.question_id, arr[0]);
    chai.assert.strictEqual(res.user_id, state.user.user_id);
    
  });
});

suite("Test de modification et de création de quizzes", function () {
  let id;
  test("On teste la création d'un quiz (newQuiz)", async function () {
    state.xApiKey = apiKey;
    await getUser();

    // On crée un quiz
    let form = new FormData();
    form.append("titre", "test automatique");
    form.append("desc", "beep boop");

    const res = await (await newQuiz(form)).json();
    id = res.quiz_id;

    chai.assert.isDefined(id);
    chai.assert.isNumber(id);
  });

  test("On teste l'ajout de question (addQuestion)", async function () {
    const form = new FormData();
    form.append("answer", "0");
    form.append("0", "Beep");
    form.append("1", "Boop");
    form.append("sentence", "bip bop ?");

    const res = await addQuestion(id, form, 0);

    chai.assert.isDefined(res);
    chai.assert.isDefined(res.quiz_id);
    chai.assert.strictEqual(id, res.quiz_id);
    chai.assert.isDefined(res.question_id);
    chai.assert.strictEqual(res.question_id, 0);
  });

  test("On teste la modification d'un quiz (updateQuiz)", async function () {
    const form = new FormData();
    form.append("status", true);
    form.append("titre", "Test automatisé");
    form.append("desc", "Beep Boop");

    const res = await (await updateQuiz(form, id)).json();

    chai.assert.isDefined(res);
    chai.assert.strictEqual(res.quiz_id, id);
  });

  test("On teste la modification de question (updateQuestion)", async function () {
    const form = new FormData();
    form.append("answer", "1");
    form.append("content", "Bip Bop ?");
    form.append("0", "Beep");
    form.append("1", "Boop");

    const res = await updateQuestion(form, id, 0);

    chai.assert.isDefined(res);
    chai.assert.strictEqual(id, res.quiz_id);
    chai.assert.strictEqual(res.question_id, 0);
  });

  test("On test la récupération des réponses (getUserQuizAnswer)", async function () {
    const res = await getUserQuizAnswer(id, 0);

    chai.assert.isDefined(res);
    chai.assert.strictEqual(id, res.quiz_id);
    chai.assert.strictEqual(res.question_id, 0);
    chai.assert.isString(res.sentence);
    chai.assert.isArray(res.propositions);
    const p = res.propositions[1];
    chai.assert.strictEqual(p.correct, true);
    chai.assert.isArray(p.answers);
  });

  test("On teste la recherche de quiz (search)", async function () {
    const res = await search("Test automatisé");

    chai.assert.isArray(res);
    
    const q = res.find(e => e.quiz_id === id);

    chai.assert.isDefined(q);
    chai.assert.strictEqual(q.type, "quiz");
  });

  test("On teste la suppression d'une question (deleteQuestion)", async function () {
    const res = await deleteQuestion(id, 0);

    chai.assert.isDefined(res);
    chai.assert.strictEqual(res.quiz_id, id);
    chai.assert.strictEqual(res.question_id, 0);
  })

  test("On teste la destruction d'un quiz (deleteQuiz)", async function () {
    const url = `${state.serverUrl}/quizzes/${id}`;
    const res = await deleteQuiz(id);

    chai.assert.strictEqual(id, Number(res.quiz_id));
  });
});
